package com.phichet.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver(){
        Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_Y);
        robot.down();
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }

    @Test
    public void shouldDownNegative(){
        Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
        robot.up();
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }

    @Test
    public void shouldUpSuccess(){
        Robot robot = new Robot("Robot", 'R', 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldDownSuccess(){
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }

    @Test
    public void shouldRightOver(){
        Robot robot = new Robot("Robot", 'R', Robot.MAX_X, 0);
        robot.right();
        assertEquals(false, robot.right());
        assertEquals(Robot.MAX_X, robot.getX());
    }
    @Test
    public void shouldleftOver(){
        Robot robot = new Robot("Robot", 'R', Robot.MIN_X, 0);
        robot.left();
        assertEquals(false, robot.left());
        assertEquals(Robot.MIN_X, robot.getX());
    }
}
