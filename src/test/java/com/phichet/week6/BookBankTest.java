package com.phichet.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void sholdWithdrawSuccess() {
        BookBank book = new BookBank("Phichet", 100);
        book.withdraw(50);
        assertEquals(50,book.getBalance(), 0.00001);
    }

    @Test
    public void sholdWithdrawOverBalance() {
        BookBank book = new BookBank("Phichet", 100);
        book.withdraw(150);
        assertEquals(100,book.getBalance(), 0.00001);
    }
    @Test
    public void sholdWithdrawONegativeNumber() {
        BookBank book = new BookBank("Phichet", 100);
        book.withdraw(-100);
        assertEquals(100,book.getBalance(), 0.00001);
    }
}
