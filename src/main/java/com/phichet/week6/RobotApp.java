package com.phichet.week6;

import java.sql.Blob;

public class RobotApp {
    public static void main(String[] args) {
        Robot kapong = new Robot("Kapong",'K',0,0);
        kapong.print();
        for(int i = 0;i <20;i++) {
            kapong.down();
            kapong.print();
        }
        
        kapong.print();

        Robot blue = new Robot("Blue", 'B', 1, 1);
        blue.print();
    }
}
