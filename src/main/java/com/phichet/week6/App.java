package com.phichet.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank phichet = new BookBank("Phichet", 50.0);
        phichet.print();
        
        BookBank prayut = new BookBank("Prayut", 100000.0);
        prayut.print();
        
        prayut.withdraw(40000.0);
        prayut.print();

        phichet.deposit(40000.0);
        phichet.print();

        BookBank prawit = new BookBank("Prawit", 100000.0);
        prawit.print();
        
        prawit.withdraw(200);
        prawit.print();
    }
}
